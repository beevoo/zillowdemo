﻿using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ZillowDemo.Wrappers
{
    public interface IHttpClientWrapper
    {
        Task<Stream> GetAsync(string apiUrl);
        HttpRequestHeaders DefaultRequestHeaders();
    }
}
