﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ZillowDemo.Wrappers
{
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private HttpClient _client = new HttpClient();

        public async Task<Stream> GetAsync(string apiUrl)
        {
            var response = await _client.GetAsync(apiUrl);
            return await _client.GetStreamAsync(apiUrl);
        }

        public HttpRequestHeaders DefaultRequestHeaders()
        {
            return _client.DefaultRequestHeaders;
        }
    }
}
