﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace ZillowDemo.Wrappers
{
    public class DataContractJsonSerializerWrapper<T> : IDataContractJsonSerializerWrapper<T>
    {
        private readonly DataContractJsonSerializer _serializer = new DataContractJsonSerializer(typeof(T));

        public object ReadObject(Stream stream)
        {
            return _serializer.ReadObject(stream);
        }
    }
}
