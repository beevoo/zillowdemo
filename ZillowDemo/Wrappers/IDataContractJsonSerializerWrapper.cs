﻿using System.IO;

namespace ZillowDemo.Wrappers
{
    public interface IDataContractJsonSerializerWrapper<T>
    {
        object ReadObject(Stream stream);
    }
}
