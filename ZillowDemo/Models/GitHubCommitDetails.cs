﻿using System;
using System.Runtime.Serialization;

namespace ZillowDemo.Models
{
    [DataContract(Name = "items")]
    public class GitHubCommitDetails
    {
        [DataMember(Name = "repository")]
        public GitHubRepositoryDetails Repository { get; set; }

        [DataMember(Name = "commit")]
        public Commit Commit { get; set; }
    }

    [DataContract(Name = "commit")]
    public class Commit
    {
        [DataMember(Name = "committer")]
        public Committer Committer { get; set; }
    }

    [DataContract(Name = "committer")]
    public class Committer
    {
        [DataMember(Name = "date")]
        public string Date { get; set; }
    }
}
