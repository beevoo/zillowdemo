﻿using System.Runtime.Serialization;

namespace ZillowDemo.Models
{
    [DataContract(Name="repository")]
    public class GitHubRepositoryDetails
    {
        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "name")]
        public string RepositoryName { get; set; }
    }
}
