﻿using System.Runtime.Serialization;

namespace ZillowDemo.Models
{
    [DataContract(Name="users")]
    public class GitHubUser
    {
        [DataMember(Name="login")]
        public string Username { get; set; }
        
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "html_url")]
        public string Url { get; set; }

        [DataMember(Name = "public_repos")]
        public int NumberOfPublicRepos { get; set; }
    }
}
