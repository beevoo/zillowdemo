﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ZillowDemo.Models
{
    [DataContract(Name = "search")]
    public class GitHubCommits
    {
        [DataMember(Name = "total_count")]
        public int NumberOfCommitsFound { get; set; }

        [DataMember(Name="items")]
        public IList<GitHubCommitDetails> Commits { get; set; }
    }
}
