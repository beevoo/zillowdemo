﻿using ZillowDemo.Models;

namespace ZillowDemo.Services
{
    public interface IGitHubService
    {
        GitHubCommits GetCommitsByUsernameAsync(string username);
        GitHubUser GetUserAsync(string username);
    }
}
