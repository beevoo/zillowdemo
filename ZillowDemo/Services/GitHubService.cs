﻿using ZillowDemo.Wrappers;
using ZillowDemo.Models;
using System.Net.Http;
using System;

namespace ZillowDemo.Services
{
    public class GitHubService : IGitHubService
    {
        private const string endpoint = "https://api.github.com";
        private readonly string USER_API_URI = string.Format("{0}/users/", endpoint);
        private readonly string COMMITS_API_URI = string.Format("{0}/search/commits", endpoint);

        private readonly IDataContractJsonSerializerWrapper<GitHubUser> _gitHubUserSerializer;
        private readonly IDataContractJsonSerializerWrapper<GitHubCommits> _gitHubCommitSerializer;
        private readonly IHttpClientWrapper _httpClient;

        public GitHubService(IHttpClientWrapper httpClientWrapper, IDataContractJsonSerializerWrapper<GitHubUser> gitHubUserSerializer,
            IDataContractJsonSerializerWrapper<GitHubCommits> gitHubCommitSerializer)
        {
            _httpClient = httpClientWrapper;
            _gitHubUserSerializer = gitHubUserSerializer;
            _gitHubCommitSerializer = gitHubCommitSerializer;

            // Needed for experimental api calls. See: https://developer.github.com/changes/2017-01-05-commit-search-api/
            _httpClient.DefaultRequestHeaders().TryAddWithoutValidation("Accept", "application/vnd.github.cloak-preview");

            // Hack to circumvent having to authenticate with api
            _httpClient.DefaultRequestHeaders().TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
        }

        public GitHubUser GetUserAsync(string username)
        {
            var response = _httpClient.GetAsync(USER_API_URI + username);

            return _gitHubUserSerializer.ReadObject(response.Result) as GitHubUser;
        }

        public GitHubCommits GetCommitsByUsernameAsync(string username)
        {
            var searchText = "+a"; // Git requires a text to search for

            var response = _httpClient.GetAsync(COMMITS_API_URI + "?q=user:" + username + searchText);

            return _gitHubCommitSerializer.ReadObject(response.Result) as GitHubCommits;
        }
    }
}
