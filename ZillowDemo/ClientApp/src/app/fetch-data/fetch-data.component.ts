import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public githubUserDetails: GitHubUserDetailsModel;
  errorMessage: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  searchGitHubUser(username: string) {
    this.errorMessage = '';
    this.githubUserDetails = null;

    if (username) {
      this.http.get<any>(`${this.baseUrl}api/GitHubUser/Details/${username}`).subscribe(result => {
        if (result.errorMessage) {
          this.errorMessage = result.errorMessage;
        } else {
          this.githubUserDetails = {
            commits: result.topCommittedToRepositories,
            details: result.details
          };
        }

      }, error => this.errorMessage = error.message);
    }
  }
}
