﻿using System.Collections.Generic;
using ZillowDemo.Models;

namespace ZillowDemo.ViewModels
{
    public class GitHubUserDetails : ResponseBase
    {
        public IEnumerable<string> TopCommittedToRepositories { set; get; }
        public GitHubUser Details { set; get; }
    }
}
