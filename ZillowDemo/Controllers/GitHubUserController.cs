﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using ZillowDemo.Models;
using ZillowDemo.Services;
using ZillowDemo.ViewModels;

namespace ZillowDemo.Controllers
{
    [Route("api/[controller]")]
    public class GitHubUserController : Controller
    {
        private readonly IGitHubService gitHubService;

        public GitHubUserController(IGitHubService gitHubService)
        {
            this.gitHubService = gitHubService;
        }

        [HttpGet("[action]/{username}")]
        public GitHubUserDetails Details(string username)
        {
            var response = new GitHubUserDetails();

            try
            {
                var commits = gitHubService.GetCommitsByUsernameAsync(username);

                response.Details = gitHubService.GetUserAsync(username);
                response.TopCommittedToRepositories = GetTopCommittedToRepositories(commits);

                return response;
            }
            catch(Exception ex)
            {
                response.ErrorMessage = ex.Message;
                return response;
            }

        }

        private IEnumerable<string> GetTopCommittedToRepositories(GitHubCommits commits)
        {
            var dictionary = new Dictionary<string, int>();

            foreach(GitHubCommitDetails commit in commits.Commits)
            {
                // Github sends back a date time format that native .net core libraries can't deserialize :(
                // So skipping the compare to get only commits within the past year.

                var repositoryName = commit.Repository.RepositoryName;

                if (dictionary.ContainsKey(repositoryName))
                {
                    dictionary[repositoryName] += 1;
                }
                else
                {
                    dictionary.Add(repositoryName, 1);
                }
            }

            var sortedList = dictionary.ToList().OrderByDescending(a => a.Value);
            return sortedList.Take(10).Select(kvp => kvp.Key);
        }
    }
}